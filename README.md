
LI, JIN-TENG
1706132631

# Tutorial & Lab Repository
Programming Foundations 2 -  | Faculty of Computer Science, University of Indonesia, Even Semester 2017/2018
***


## Table of Content

This repository will contain Tutorial & Lab materials from Programming Foundations 2.

1. Lab
    1. [Lab 1](https://gitlab.com/DDP2-CSUI/ddp-lab-ki/blob/master/lab_instructions/lab_1/README.md) - Introduction to Java & Git
    2. [Lab 2](https://gitlab.com/DDP2-CSUI/ddp-lab-ki/blob/master/lab_instructions/lab_2/README.md) - Basic concepts of Java Programming

***

Tools that will be used in this courses are :

- Java Development Kit (JDK) 8
- Git
- Notepad++ (or similar text editor)
- Integrated Development Environment (IDE)
- Gradle
- GitLab Account

Make sure that you install and have the needed tools above, see this manual for installation and configuration if you haven't installed it [here](https://drive.google.com/file/d/1c1AA-9ju1S82-NYyV7EMyPNwScPpMQsr/view?usp=sharing)

Contact Information :

- Line Dek Depe : [@nhz2170m](https://line.me/R/ti/p/%40nhz2170m)
>>>>>>> 7b32de54ecb2a566ce1959dc73eaf322d205eb09
